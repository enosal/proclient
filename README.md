proClient
=========

This is a JavaFX Client Application that uses the Last.FM API to search for and display artists or songs. Clicking on an artist displays their songs while clicking on a song opens up that Last.FM webpage.

Advanced Java, Summer 2016