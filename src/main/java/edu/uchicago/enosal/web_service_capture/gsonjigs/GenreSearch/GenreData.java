
package edu.uchicago.enosal.web_service_capture.gsonjigs.GenreSearch;

import javax.annotation.Generated;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

@Generated("org.jsonschema2pojo")
public class GenreData {
    @SerializedName("topartists")
    @Expose
    private Topartists topartists;

    //Methods
    public Topartists getTopartists() {
        return topartists;
    }
    public void setTopartists(Topartists topartists) {
        this.topartists = topartists;
    }

    //Subclass
    public class Topartists {
        @SerializedName("artist")
        @Expose
        private List<Artist> artist = new ArrayList<>();

        //METHODS
        public List<Artist> getArtistList() {
            return artist;
        }
        public void setArtistList(List<Artist> artist) {
            this.artist = artist;
        }


        //SUBCLASS
        public class Artist {
            @SerializedName("name")
            @Expose
            private String name;
            @SerializedName("url")
            @Expose
            private String url;
            @SerializedName("streamable")
            @Expose
            private String streamable;

            //METHODS
            public String getName() {
                return name;
            }
            public void setName(String name) {
                this.name = name;
            }

            public String getUrl() {
                return url;
            }
            public void setUrl(String url) {
                this.url = url;
            }

            public String getStreamable() {
                return streamable;
            }
            public void setStreamable(String streamable) {
                this.streamable = streamable;
            }

        } //End Artist

    }//End Topartist

    //Returns arraylist of strings (for artist)
    public ArrayList<String> getArtists() {
        ArrayList<String> artistStringList = new ArrayList<>();


        for (Topartists.Artist artist : getTopartists().getArtistList()) {
            String trackInfo = artist.getName();
            artistStringList.add(trackInfo);
        }

        return artistStringList;
    }




}//End GenreData
