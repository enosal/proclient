
package edu.uchicago.enosal.web_service_capture.gsonjigs.ArtistSearch;

import javax.annotation.Generated;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/*
    Nested classes to accommodate the jested JSON object
    ArtistData contains the Toptracks class.
    Toptracks contains the Track class. Toptracks has an instance variable that is an arraylist of Track

    getTracks() outputs an arraylist of String
 */

@Generated("org.jsonschema2pojo")
public class ArtistData {
    @SerializedName("toptracks")
    @Expose
    private Toptracks toptracks = new Toptracks();


    //METHODS
    public Toptracks getToptracks() {return toptracks;}
    public void setToptracks(Toptracks toptracks) {this.toptracks = toptracks;  }

    //SUBCLASS
    public class Toptracks {
        @SerializedName("track")
        @Expose
        private List<Track> tracklist = new ArrayList<>();


        //METHODS
        public List<Track> getTrackList() {return tracklist; }
        public void setTrackList(List<Track> tracklist) {this.tracklist = tracklist; }

        //SUBCLASS
        public class Track {
            @SerializedName("name")
            @Expose
            private String name;
            @SerializedName("playcount")
            @Expose
            private String playcount;
            @SerializedName("listeners")
            @Expose
            private String listeners;
            @SerializedName("url")
            @Expose
            private String url;
            @SerializedName("streamable")
            @Expose
            private String streamable;
            @SerializedName("artist")
            @Expose
            private Artist artist;

            //METHODS
            public String getName() {
                return name;
            }
            public void setName(String name) {this.name = name;
            }

            public String getPlaycount() {
                return playcount;
            }
            public void setPlaycount(String playcount) {
                this.playcount = playcount;
            }

            public String getListeners() {
                return listeners;
            }
            public void setListeners(String listeners) {
                this.listeners = listeners;
            }

            public String getUrl() {
                return url;
            }
            public void setUrl(String url) {
                this.url = url;
            }

            public String getStreamable() {
                return streamable;
            }
            public void setStreamable(String streamable) {
                this.streamable = streamable;
            }

            public String getArtist() {
                return artist.getName();
            }
            public void setArtist(Artist artist) {
                this.artist = artist;
            }

        }//End Track

    } //End Toptracks

    //Returns an arraylist of strings (for tracks)
    public ArrayList<String> getTracks() {
        ArrayList<String> trackStringList = new ArrayList<>();


        for (Toptracks.Track track : getToptracks().getTrackList()) {
            String trackInfo = track.getArtist() + " - " + track.getName() + " | " + track.getPlaycount() + " plays, " + track.getListeners() + " listeners";
            trackStringList.add(trackInfo);
        }

        return trackStringList;
    }

} //End ArtistData
