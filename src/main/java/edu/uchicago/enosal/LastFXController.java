package edu.uchicago.enosal;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;
import java.awt.*;
import java.net.URI;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.concurrent.Service;
import javafx.concurrent.Task;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.text.Text;


/**
 * FXML Controller class
 * Adapted from proclient base code
 */

public class LastFXController implements Initializable {
    public static final int ANCHOR_PANE_WIDTH = 650;
    public static final int ANCHOR_PANE_HEIGHT = 530;

    @FXML
    private Button btnSearch;

    @FXML
    private ListView<?> listView;

    @FXML
    private Pane paneSearch;

    @FXML
    private ChoiceBox choiceSearch;

    @FXML
    private AnchorPane paneMain;

    @FXML
    private Label lblWelcome;

    @FXML
    private Label lblStatus;

    @FXML
    private TextField txtFieldSearch;


    //For searching
    private String searchTerm = "";
    private boolean searchForArtist = true;

    //For listview
    ArrayList<String> lastFXSearchResults;



    //Initialize Controller class
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        ////////////
        //CREATING//
        ////////////

        //Create LastFX
        LastFX lastFX = new LastFX();

        //Create Service
        LastFXFinder lastFXFinder = new LastFXFinder(lastFX);

        //Create choicebox
        choiceSearch.setItems(FXCollections.observableArrayList("Artists", "Genres"));
        choiceSearch.getSelectionModel().select(0);
        choiceSearch.setTooltip(new Tooltip("Select whether to search for Artists or Genres"));

        ///////////
        //BINDING//
        ///////////

        lblStatus.textProperty().bind(lastFXFinder.messageProperty());
        btnSearch.disableProperty().bind(lastFXFinder.runningProperty());
        listView.itemsProperty().bind(lastFXFinder.valueProperty());

        /////////////
        //LISTENING//
        /////////////

        //Search choicebox listener
        choiceSearch.getSelectionModel().selectedIndexProperty().addListener(new ChangeListener<Number>() {
            public void changed(ObservableValue ov, Number value, Number new_value) {
                if ((Integer) new_value == 0) {
                    setSearchForArtist(true);
                } else {
                    setSearchForArtist(false);
                }
            }
        }); //End choicebox listener


        //Search button listener
        btnSearch.setOnAction(new EventHandler<ActionEvent>() {
            @Override public void handle(ActionEvent actionEvent) {
                setSearchTerm(txtFieldSearch.getText());

                if (!lastFXFinder.isRunning()) {
                    lastFXFinder.reset();
                    lastFXFinder.start();
                }
            }
        }); //end button listener

        //Listview click action
        listView.setOnMouseClicked(me -> {
            if (!(me.getTarget() instanceof Text)) {
                return;
            }

            String strClicked = ((Text) me.getTarget()).getText().toString();

            //ARTISTS - Display webpages upon a click
            if (searchForArtist) {

                String[] stringParts = strClicked.split("\\|");
                String relevantPart = stringParts[0];
                String[] moreParts = relevantPart.split("-");
                String artistPart = moreParts[0];
                String songPart = moreParts[1];
                String formattedArtist = artistPart.trim().replaceAll(" ", "+");
                String formattedSong = songPart.trim().replaceAll(" ", "+");

                try {
                    //EX: http://www.last.fm/music/The+Appleseed+Cast/_/Innocent+Vigilant+Ordinary
                    openWebpage(new URI("http://www.last.fm/music/" + formattedArtist + "/_/" + formattedSong));

                } catch (Exception e) {
                    System.out.println("Unable to open webpage");
                }
            } //End if for Artist search

            //GENRES - display the artist's songs in the listview upon a click
            else {
                if (!lastFXFinder.isRunning()) {
                    lastFXFinder.reset();
                    setSearchTerm(strClicked);
                    setSearchForArtist(true);
                    lastFXFinder.start();
                }//End Running if

            }//End If/else for Genre Search

        }); //End listview listener



    } //End initialize method


    //Searching Getters & Setters
    public boolean getSearchForArtist() {return searchForArtist;}
    public void setSearchForArtist(boolean searchForArtist) {this.searchForArtist = searchForArtist; }
    public String getSearchTerm() {return searchTerm;}
    public void setSearchTerm(String searchTerm) {this.searchTerm = searchTerm;   }


    //Opens webpage
    public static void openWebpage(URI uri) {
        Desktop desktop = Desktop.isDesktopSupported() ? Desktop.getDesktop() : null;
        if (desktop != null && desktop.isSupported(Desktop.Action.BROWSE)) {
            try {
                desktop.browse(uri);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    } //End openWebpage method


    /////////////////
    //SERVICE CLASS//
    /////////////////

    final class LastFXFinder extends Service {
        //FIELDS
        private LastFX lastfx;

        //CONSTRUCTOR
        LastFXFinder(LastFX lastFX) {
            this.lastfx = lastFX;
        }

        //METHODS
        @Override
        public Task createTask() {
            return new Task<ObservableList<String>>() {
                @Override
                protected ObservableList<String> call() throws InterruptedException {
                    updateMessage("Status: Searching Last.FM . . . ");


                    lastFXSearchResults = lastfx.searchForArtist(getSearchTerm(), getSearchForArtist());


                    if (lastFXSearchResults.size() == 0) {
                        updateMessage("Status: Nothing was found.");
                    } else {
                        updateMessage("Status: Top 10 results below.");
                    }


                    return FXCollections.observableArrayList(lastFXSearchResults);
                } //End call() method
            }; //end the Task<ObservableList<String>> return
        } //End createTask method

    } //End LastFXFinder class

} //End LastFXController






