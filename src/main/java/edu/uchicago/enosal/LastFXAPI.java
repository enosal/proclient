package edu.uchicago.enosal;

/**
 * Created by Eryka on 7/9/2016.
 */
import org.scribe.model.Token;
import org.scribe.builder.api.DefaultApi10a;

/**
 * Service provider for "2-legged" OAuth10a for Yelp API (version 2).
 * Taken from proClient Yelp base code
 */
public class LastFXAPI extends DefaultApi10a {

    @Override
    public String getAccessTokenEndpoint() {
        return null;
    }
    @Override
    public String getAuthorizationUrl(Token arg0) {return null;}
    @Override
    public String getRequestTokenEndpoint() {
        return null;
    }

}