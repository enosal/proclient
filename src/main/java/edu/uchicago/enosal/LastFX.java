package edu.uchicago.enosal;

import com.google.gson.Gson;

import edu.uchicago.enosal.web_service_capture.gsonjigs.GenreSearch.GenreData;
import org.scribe.builder.ServiceBuilder;
import org.scribe.model.OAuthRequest;
import org.scribe.model.Response;
import org.scribe.model.Token;
import org.scribe.model.Verb;
import org.scribe.oauth.OAuthService;
import edu.uchicago.enosal.web_service_capture.gsonjigs.ArtistSearch.ArtistData;

import java.util.ArrayList;


/**
 * Accesses Last.FM's API
 */
public class LastFX {
    private static final String API_KEY = "762a9a406bf52afa6195cb4f55e17d2e";
    private static final String SHARED_SECRET = "acad13c6fb9b020be8b7d5898194c1c2";

    OAuthService service;
    Token accessToken;

    public LastFX() {
        this.service = new ServiceBuilder().provider(LastFXAPI.class).apiKey(API_KEY).apiSecret(SHARED_SECRET).build();
        this.accessToken = new Token(API_KEY, SHARED_SECRET);

    }

    public ArrayList<String> searchForArtist(String searchTerm, boolean searchForArtists) {
        //Set up credentials
        OAuthService service = new ServiceBuilder().provider(LastFXAPI.class).apiKey(API_KEY).apiSecret(SHARED_SECRET).build();
        Token accessToken = new Token(API_KEY, SHARED_SECRET);
        OAuthRequest request = new OAuthRequest(Verb.GET, "http://ws.audioscrobbler.com/2.0/");

        //Search results variable
        ArrayList<String> searchResults = new ArrayList<>();

        //Response
        Response response;
        String rawData;

        //ARTISTS
        if (searchForArtists) {
            //Set up request
            request.addQuerystringParameter("method", "artist.getTopTracks");
            request.addQuerystringParameter("artist", searchTerm);
            request.addQuerystringParameter("autocorrect", "1");
        }
        //TAGS
        else {
            request.addQuerystringParameter("method", "tag.getTopArtists");
            request.addQuerystringParameter("tag", searchTerm);
        }

        //Request info that is the same for both
        request.addQuerystringParameter("api_key", API_KEY);
        request.addQuerystringParameter("format", "json");
        request.addQuerystringParameter("limit", "10");

        //Send request & receive response
        service.signRequest(accessToken, request);
        response = request.send();
        rawData = response.getBody();


        //Put results into ArrayList<String> format for ObservableList
        try {
            if (searchForArtists) {
                ArtistData artistData = new Gson().fromJson(rawData, ArtistData.class);
                searchResults = artistData.getTracks();
            }
            else {
                GenreData genreData = new Gson().fromJson(rawData, GenreData.class);
                searchResults = genreData.getArtists();
            }
        } catch (Exception e) {
            System.out.println("Something went wrong in the arraylist conversion");
        }


        return searchResults;
    }

}
