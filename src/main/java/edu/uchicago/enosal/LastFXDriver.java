package edu.uchicago.enosal;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Group;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.scene.media.MediaView;
import javafx.stage.Stage;

/**
 * LastFXDriver is the driver class that launches the application
 */
public class LastFXDriver extends Application {

    public static void main(String[] args) throws Exception {
//        LastFX startLastFX = new LastFX();
//        startLastFX.searchForArtist("The Appleseed Cast");

        launch(args);
    }

    public void start(Stage stage) throws Exception {
        //FXML
        String fxmlFile = "/fxml/lastfx.fxml";
        FXMLLoader loader = new FXMLLoader();
        Parent rootNode = (Parent) loader.load(getClass().getResourceAsStream(fxmlFile));

        //Create & set the Scene
        Scene scene = new Scene(rootNode, LastFXController.ANCHOR_PANE_WIDTH, LastFXController.ANCHOR_PANE_HEIGHT);
        scene.getStylesheets().add("/styles/styles.css");
        stage.setTitle("LastFX");
        stage.setScene(scene);
        stage.show();

    }
}
